package bankdemo.java.dao;

import bankdemo.java.models.Account;

public interface AccountDAO extends ItemDAO<Account> {

}
