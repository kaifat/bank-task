package bankdemo.java.dao;

import java.util.List;

import bankdemo.java.models.Model;

public interface ItemDAO<T extends Model> {
	
	public List<T> getAll();
	
	public T getById(Long id);
	
	public void add(T model);
	
	public void update(T model);
	
	public void delete(T model);
	
}
