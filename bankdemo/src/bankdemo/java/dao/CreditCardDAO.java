package bankdemo.java.dao;

import bankdemo.java.models.CreditCard;

public interface CreditCardDAO extends ItemDAO<CreditCard> {
	
}
