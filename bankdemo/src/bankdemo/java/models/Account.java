package bankdemo.java.models;

public class Account extends Model {
	
	private static final long serialVersionUID = 1860554298002133192L;
	
	private String firstname;
	private String lastname;
	private String country;
	
	public Account(){
		super();
	}
	
	public Account(Long id){
		super(id);
	}


	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}
