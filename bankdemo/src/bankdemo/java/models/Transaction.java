package bankdemo.java.models;

public class Transaction extends Model{
	
	private static final long serialVersionUID = 3963954499543156536L;

	
	private CreditCard from�ard;
	private CreditCard to�ard;
	private int amount;
	
	public Transaction(){
		super();
	}
	
	public Transaction(Long id){
		super(id);
	}

	public CreditCard getFrom�ard() {
		return from�ard;
	}

	public void setFrom�ard(CreditCard from�ard) {
		this.from�ard = from�ard;
	}

	public CreditCard getTo�ard() {
		return to�ard;
	}

	public void setTo�ard(CreditCard to�ard) {
		this.to�ard = to�ard;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	

}
