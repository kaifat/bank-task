package bankdemo.java.models;

public class CreditCard extends Model {
	
	
	private static final long serialVersionUID = -3092406032072090029L;
	
	private Account account;
	private int cash;
	
	public CreditCard(){
		super();
	}
	
	public CreditCard(Long id){
		super(id);
	}
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}
	
	

}
